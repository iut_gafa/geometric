#include "../headers/GlobalFormesManager.hpp"

//Constructeur
GlobalFormesManager::GlobalFormesManager(sf::RenderWindow * window, std::string fileName)
: _selected(nullptr), _whoHaveIt(nullptr), _window(window), _isCreating(false), _points(new std::vector<Point>())
{

    charger(fileName);
}

GlobalFormesManager::~GlobalFormesManager() {
/*  std::list<Formes*>::const_iterator lit(_chargedFormes.begin()), lend(_chargedFormes.end());
  for (; lit != lend; ++lit) {
    delete lit;
  }*/
}

void GlobalFormesManager::drawAll()
{
    for (DrawableFormes* currentFormes : _chargedFormes)
    {
    currentFormes -> draw(*_window);
    }

    if (_isCreating)
    {
        for (size_t i = 0; i < _points->size(); i++)
        {
          DrawablePoint tmp(_points->at(i).getX(), _points->at(i).getY());
          tmp.draw(*_window);
        }
    }
}


void GlobalFormesManager::ajouter(Drawable* forme) {
  _chargedFormes.push_back(new DrawableFormes(1));
  DrawableFormes* f = *_chargedFormes.rbegin();
  f -> ajouter(forme);
}

void GlobalFormesManager::save(std::string fileName) {
  //On ouvre le flux
  ofstream save(fileName);

  //On écrit dedans
  for (DrawableFormes* currentFormes : _chargedFormes) {
    save << "new" << endl;
    save << *currentFormes;
  }
}

void GlobalFormesManager::charger(std::string fileName) {
  //On ouvre le flux
  ifstream save(fileName);

  //On charge ce qu'il y a dans le fichier
  //On commence par initialisé test
  std::string test;
  save >> test;

  //On fait une boucle qui va continuer tant qu'un new est trouvé.
  while (test == "new") {
    //On charge une nouvelle forme
    _chargedFormes.push_back(new DrawableFormes);
    DrawableFormes* f = *(_chargedFormes.rbegin());
    f -> charger(save);

    //On actualise la valeur de test pour éviter une boucle infini.
    test = "";
    save >> test;
  }
}

void GlobalFormesManager::checkPosition(sf::Vector2i coord)
{
    if (_selected != nullptr)
    {
        _selected->setSelected(false);
    }
    _whoHaveIt = nullptr;
    _selected = nullptr;


	for(std::vector<DrawableFormes*>::iterator lit = _chargedFormes.begin(); _selected == nullptr && !(_chargedFormes.empty()) && lit != _chargedFormes.end(); ++lit) {
		_selected = (*lit) -> isOver(coord.x, coord.y);
		if (_selected != nullptr)
		_whoHaveIt = *lit;
	}
	if (selected()) {
		_selected -> setSelected(true);
        // On remet à zéro la création :
        _points->clear();
        _isCreating = false;
	}
}

void GlobalFormesManager::moveTo(sf::Vector2i mousePosition) {
	_selected -> moveTo(mousePosition);
    /*
	_selected -> setSelected(false);
	_selected = nullptr;
	_whoHaveIt = nullptr;
    */
}

//Il faut optimiser la boucle et permettre la suppression d'une forme en particulier.
void GlobalFormesManager::deleteCurrentForme() {
  std::vector<DrawableFormes*>::iterator lit = _chargedFormes.begin();
  while (*lit != _whoHaveIt) {
    ++lit;
  }
  delete (*lit);
  _chargedFormes.erase(lit);
  _selected = nullptr;
  _whoHaveIt = nullptr;
}

void GlobalFormesManager::mouseRightClicked()
{
    if (selected())
    {
		moveTo(sf::Mouse::getPosition(*_window));
	}
}

void GlobalFormesManager::mouseLeftClicked()
{

    if (_isCreating)
    {
        _points->push_back(Point(sf::Mouse::getPosition(*_window).x, sf::Mouse::getPosition(*_window).y));
    }
    else
    { //Sinon
		checkPosition(sf::Mouse::getPosition(*_window));
		//On essaye d'en sélectionner une
	}

}

void GlobalFormesManager::keyPressed(sf::Vector2i pos, char key) {
	//Cas où une forme est sélectionner
	if (_selected == nullptr && _whoHaveIt == nullptr) {
		switch (key) {
			case 'c': ajouter(new DrawableSquare(sf::Color::Black, 100, pos.x, pos.y)); break;
			case 'r': ajouter(new DrawableRectangle(sf::Color::Black, 100, 50, pos.x, pos.y)); break;
			case 's': ajouter(new DrawableCircle(sf::Color::Black, 50, pos.x, pos.y)); break;
			case 'e': ajouter(new DrawableEllipsis(sf::Color::Black, 50, 100, pos.x, pos.y)); break;
			case 't': ajouter(new DrawableTriangle(sf::Color::Black, new Point(50, 0), new Point(0, 50), new Point(100, 50), pos.x, pos.y)); break;
      case 'p': swaPolygone(); break;
			case 'o': swaOvale(); break;
      case 'i':
				std::cout << "Donnez le chemin d'accès vers l'image" << std::endl << '>';
				std::string imgPath;
				std::cin >> imgPath;
				ajouter (new DrawableImage(imgPath, 200, 200, pos.x, pos.y));
			break;
		} //Si aucune forme n'est sélectionner
	} else {
		switch (key) {
			case 'd': deleteCurrentForme(); break;
			default : editForme(key); break;
		}
	}
}

void GlobalFormesManager::editForme(char& key) {
	switch (key) {
		//On commence par les raccourcie généraux-----------------------------------
		//Si l'on souhaite changer la couleur
		case 'k':
			int r, g, b;
			std::cout << "Couleur actuel (r=" << _selected->getColor().toInteger() << ")" << endl;
			std::cout << "Saisissez les composante r, v, b d'une couleur (dans cet ordre) >";
			std::cin >> r >> g >> b;
			_selected->setColor(sf::Color(r, g, b));
		break;

        case '1':
            _selected->setAnimEnabled(0, !_selected->isAnimEnabled(0));
        break;

        case '2':
            _selected->setAnimEnabled(1, !_selected->isAnimEnabled(1));
        break;

        case '3':
            _selected->setAnimEnabled(2, !_selected->isAnimEnabled(2));
        break;

        case '4':
            _selected->setAnimEnabled(3, !_selected->isAnimEnabled(3));
        break;

        case '+':
            if (_selected->getThickness() <= 100)
                _selected->setThickness(_selected->getThickness()+1);
        break;

        case '-':
            if (_selected->getThickness() > 0)
                _selected->setThickness(_selected->getThickness()-1);
        break;

        case 'f':
            _selected->setEnabled(_selected->getColor().a == 0);
        break;

		//Si l'on souhaite modifier les coordonnées
		case 'x':
			int x, y;
			std::cout << "Coordonnées actuel (x=" << _selected->getAncre()->getX() << " y=" << _selected->getAncre()->getY() << ")" << endl;
			std::cout << "Saisissez les coordonnées x et y (dans cet ordre) >";
			std::cin >> x >> y;
			_selected->getAncre()->setXY(x, y);
		break;

		//Changer le(s) coté(s)
		case 'c':
		//Côté d'un carré
			if (dynamic_cast<DrawableSquare*>(_selected) != nullptr) {
				DrawableSquare* p = dynamic_cast<DrawableSquare*>(_selected);
				int cote;
				std::cout << "cote actuel (" << p->getCote() << ")" << endl;
				std::cout << "saisir une nouvelle cote >";
				std::cin >> cote;
				p->setCote(cote);
		//Côté d'un rectangle
			} else if (dynamic_cast<DrawableRectangle*>(_selected)){
				DrawableRectangle* p = dynamic_cast<DrawableRectangle*>(_selected);
				int largeur, hauteur;
				std::cout << "actuel: largeur=" << p->getLargeur() << " hauteur=" << p->getHauteur() << endl;
				std::cout << "Saisir nouvelles largeur, hauteur (dans cet ordre) >";
				std::cin >> largeur >> hauteur;
				p->setLargeur(largeur);
				p->setHauteur(hauteur);
		//Côté d'un triangle
			} else if (dynamic_cast<DrawableTriangle*>(_selected) != nullptr) {
				DrawableTriangle* p = dynamic_cast<DrawableTriangle*>(_selected);
				uint Npoint, x, y;
				std::cout << "coordonnées actuels par rapport à l'ancre p0 ->" << *(p->getPoint(0)) << " p1 ->" << *(p->getPoint(1)) << " p2 ->" << *(p->getPoint(2)) << endl;
				std::cout << "Donnez le point auquel vous voulez accéder >";
				std::cin >> Npoint;
				while (Npoint > 2) {
					std::cout << "Le numéro doit être compris entre 0 et 2 >";
					std::cin >> Npoint;
				}
				std::cout << "Donnez les nouveau coordonnées x, y (dans cet ordre) >";
				std::cin >> x >> y;
				p->getPoint(Npoint)->setXY(x, y);
		//Côté d'une image
			} else if (dynamic_cast<DrawableImage*>(_selected)) {
				DrawableImage* p = dynamic_cast<DrawableImage*>(_selected);
				int largeur, hauteur;
				std::cout << "actuel: largeur=" << p->getWidth() << " hauteur=" << p->getHeight() << endl;
				std::cout << "Saisir nouvelles largeur, hauteur (dans cet ordre) >";
				std::cin >> largeur >> hauteur;
				p->setWidth(largeur);
				p->setHeight(hauteur);

			} else {
				cout << "Cette forme ne peux pas être modifier avec ce raccourcie" << endl;
			}
		break;

		//Pour les paramètres spécifiques-------------------------------------------
		//Changer le(s) rayon(s)
		case 'r':
			if (dynamic_cast<DrawableCircle*>(_selected) != nullptr) {
				DrawableCircle* p = dynamic_cast<DrawableCircle*>(_selected);
				int rayon;
				std::cout << "Rayon actuel (" << p->getRayon() << ")" << endl;
				std::cout << "Saisir nouveau Rayon >";
				std::cin >> rayon;
				p->setRayon(rayon);
			} else if (dynamic_cast<DrawableEllipsis*>(_selected) != nullptr) {
				DrawableEllipsis* p = dynamic_cast<DrawableEllipsis*>(_selected);
				int demiL, demiH;
				std::cout << "Actuel : demi largeur=" << p->getDemiLargeur() << " demi hauteur=" << p->getDemiHauteur() << endl;
				std::cout << "Saisir nouvelles demi-largeur et demi-hauteur (dans cet ordre) >";
				std::cin >> demiL >> demiH;
				p->setDemiLargeur(demiL);
				p->setDemiHauteur(demiH);
			} else {
				cout << "Cette forme ne peux pas être modifier avec ce rccourcie" << endl;
			}
		break;
	}
}


void GlobalFormesManager::swaPolygone()
{
    if (!selected())
    {
        if (_isCreating) {
					switch (_points->size()) {
						case 0:
						break;
						case 1:
							int cote;
							std::cout << "Donnez une côte >";
							std::cin >> cote;

							ajouter(new DrawableSquare(sf::Color::Black, cote, _points->at(0).getX(), _points->at(0).getY()));
						break;

						case 2:
							ajouter(new DrawableRectangle(sf::Color::Black, std::abs(int(_points->at(1).getX() - _points->at(0).getX())), std::abs(int(_points->at(1).getY() - _points->at(0).getY())), _points->at(0).getX(), _points->at(0).getY()));
						break;

						case 3:
							ajouter(new DrawableTriangle(sf::Color::Black, new Point(0, 0), new Point(std::abs(int(_points->at(1).getX() - _points->at(0).getX())), std::abs(int(_points->at(1).getY() - _points->at(0).getY()))), new Point(std::abs(int(_points->at(2).getX() - _points->at(0).getX())), std::abs(int(_points->at(2).getY() - _points->at(0).getY()))), _points->at(0).getX(), _points->at(0).getY()));
						break;

						default:
							ajouter(new DrawablePolygone(sf::Color::Black, *_points, 0, 0));
							_points->clear();
						break;
					}
        }
				_points->clear();
        _isCreating = !_isCreating;
    }
}

void GlobalFormesManager::swaOvale()
{
    if (!selected())
    {
        if (_isCreating) {
					switch (_points->size()) {
						case 0:
						break;
						case 1:
							int rayon;
							std::cout << "Donnez un rayon >";
							std::cin >> rayon;

							ajouter(new DrawableCircle(sf::Color::Black, rayon, _points->at(0).getX(), _points->at(0).getY()));
						break;

						case 2:
							ajouter(new DrawableEllipsis(sf::Color::Black, std::abs(int(_points->at(1).getX() - _points->at(0).getX()))/2, std::abs(int(_points->at(1).getY() - _points->at(0).getY()))/2, _points->at(0).getX(), _points->at(0).getY()));
						break;

						case 3:
							ajouter(new DrawableTriangle(sf::Color::Black, new Point(0, 0), new Point(std::abs(int(_points->at(1).getX() - _points->at(0).getX())), std::abs(int(_points->at(1).getY() - _points->at(0).getY()))), new Point(std::abs(int(_points->at(2).getX() - _points->at(0).getX())), std::abs(int(_points->at(2).getY() - _points->at(0).getY()))), _points->at(0).getX(), _points->at(0).getY()));
						break;

						default:
							ajouter(new DrawablePolygone(sf::Color::Black, *_points, 0, 0));
							_points->clear();
						break;
					}
        }
				_points->clear();
        _isCreating = !_isCreating;
    }
}

/*
void GlobalFormesManager::aboardCreation()
{

}*/
