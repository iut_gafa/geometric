//main.cpp
#include "headers/exe.hpp"
#include "headers/GlobalFormesManager.hpp"

int main(int argc, char** argv)
{
  //Declare---------------------------------------------------------------------
  std::string fileName = argc > 1 ? argv[1] : "save.txt";
  GlobalFormesManager* uniqueFormesManager = nullptr;


  sf::RenderWindow* window = new sf::RenderWindow(sf::VideoMode(800, 600), "Geometric");//La fenêtre
  //sf::RenderWindow* menu = new sf::RenderWindow(sf::VideoMode(300, 200), "Config Forme...");

  uniqueFormesManager = new GlobalFormesManager(window, fileName);

  // on fait tourner le programme jusqu'à ce que la fenêtre soit fermée
	sf::Clock rapidFireGuard; //Est utile pour éviter de faire des click multiple si le click reste enfoncé une frame de plus
  while (window -> isOpen()) {

    window -> clear(sf::Color::White);
    //menu -> clear(sf::Color::White);
    // on inspecte tous les évènements de la fenêtre qui ont été émis depuis la précédente itération
    sf::Event event;
    while (window -> pollEvent(event)) {
      // évènement "fermeture demandée" : on ferme la fenêtre
      switch (event.type) {
        case sf::Event::Closed:
          uniqueFormesManager -> save(fileName);
          window -> close();
        break;
				case sf::Event::TextEntered:
					uniqueFormesManager -> keyPressed(sf::Mouse::getPosition(*window), static_cast<char>(event.text.unicode));
				break;

        default:
        break;
      }

      if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && rapidFireGuard.getElapsedTime().asMilliseconds() > 200) {
				uniqueFormesManager -> mouseLeftClicked();
				rapidFireGuard.restart();
			}

        if (sf::Mouse::isButtonPressed(sf::Mouse::Right) && rapidFireGuard.getElapsedTime().asMilliseconds() > 200)
        {
            uniqueFormesManager -> mouseRightClicked();
            rapidFireGuard.restart();
        }


      //menu -> display();
    }

    uniqueFormesManager -> drawAll();
    window -> display();
  }
  //save << rect << std::endl; //Fonctionne
  //formes.sauver(save); //Fonctionne
  delete uniqueFormesManager;

  return EXIT_SUCCESS;
}
