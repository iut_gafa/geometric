#pragma once
//include-----------------------------------------------------------------------
//my lib
#include "../../include/lib_formes/headers/forme.hpp"
#include "../../include/lib_formes/headers/rectangle.hpp"
#include "../../include/lib_formes/headers/ellipse.hpp"
#include "../../include/lib_formes/headers/formes.hpp"
//System
#include <iostream>
#include <fstream>
//SFML
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

//functions---------------------------------------------------------------------

void exe(sf::RenderWindow w);
void initFormes(Formes* tab, Formes* formes); //Cette fonction initialise un état initiale du programme
