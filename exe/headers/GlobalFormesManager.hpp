#pragma once
#include "exe.hpp"
//System
#include <vector>
#include <fstream>
#include <cmath>
//My lib
#include "lib_drawableFormes/headers/DrawableFormes.hpp"
#include "lib_drawableFormes/headers/DrawableSquare.hpp"
#include "lib_drawableFormes/headers/DrawableCircle.hpp"
#include "lib_drawableFormes/headers/DrawableRectangle.hpp"
#include "lib_drawableFormes/headers/DrawableEllipsis.hpp"
#include "lib_drawableFormes/headers/DrawableTriangle.hpp"
#include "lib_drawableFormes/headers/DrawablePolygone.hpp"
#include "lib_drawableFormes/headers/DrawableImage.hpp"
#include "lib_drawableFormes/headers/DrawablePoint.hpp"

/// Permet de gérer des formes dessinables sur une fenêtre
class GlobalFormesManager
{
    Drawable* _selected; //Pointe vers la forme selectionner.
    DrawableFormes* _whoHaveIt; //Pointe sur la formes qui contient celle qui est sélectionner
    std::vector<DrawableFormes*> _chargedFormes; //Tableau de toutes les formes instancier.
    sf::RenderWindow * _window;
    bool _isCreating;
    std::vector<Point> * _points;

private:
    void swaPolygone();//Permet de dessiner des polygone à partir de point.
		void swaOvale(); //Permet de dessiner des rond avec des points.
    void aboardCreation();


public:
    /*! Construit un GlobalFormesManager à partir d'un nom de fichier et d'une fenêtre.
     *  @param[in, out] window correspond à la fenêtre.
     *  @param[in] fileName correspond au nom du fichier.
     */
    GlobalFormesManager(sf::RenderWindow * window, std::string fileName = "save.txt");

    //Destructeur
    ~GlobalFormesManager();

    //Getteur
    inline bool selected() {return !(_selected == nullptr); };
    inline DrawableFormes const* whoHaveIt() {return _whoHaveIt; };

    //Methode
    /*! Permet de dessiner toutes les formes en mémoire.*/
    void drawAll(); //Appel la fonction draw de toutes les formes instanciées
    /*! Permet d'ajouter une forme.*/
    void ajouter(Drawable* forme /** [in, out] correspond à la forme à ajouter. */); //Permet d'ajouter une formes
    /*! Permet de sauvegarder les formes.*/
    void save(std::string save /** [in] correspond au fichier. */); //Permet de sauvegarder toutes les formes
    /*! Permet de charger les formes d'un fichier.*/
    void charger(std::string save /** [in] correspond au fichier. */); //Permet de lire le fichier passé en paramètres
    /*! Permet de vérfier les formes à une position.*/
    void checkPosition(sf::Vector2i coord /** [in] correspond aux coordonnées. */); //Permet de retenir la forme sur laquelle on a clické
    /*! Permet de déplacer une forme.*/
    void moveTo(sf::Vector2i coord /** [in] correspond aux coordonnées. */); //Déplace la forme sélectionner au coordonnées passé en paramètre
    /*! Permet de supprimer une forme.*/
    void deleteCurrentForme(); //Permet de supprimer la forme sélectionner
    /*! Permet de modifier une forme.*/
    void editForme(char & key ); //Permet de modifier une forme en fonction des traitement associer à celle-ci

	//Interaction
    /*! Permet de lancer la sélection d'une forme.*/
	void mouseLeftClicked(); //Action à faire lorsque l'on clique
    /*! Permet de lancer le déplacement d'une forme.*/
    void mouseRightClicked();
    /*! Est appelée lorsque l'utilisateur appuie sur une touche.*/
	void keyPressed(sf::Vector2i pos, char key); //Action à faire lorsque l'on appuie sur une touche
};
