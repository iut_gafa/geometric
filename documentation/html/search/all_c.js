var searchData=
[
  ['save',['save',['../dc/db5/classGlobalFormesManager.html#ac65ba6209308c23b6bded447e77e2762',1,'GlobalFormesManager']]],
  ['setfixscale',['setFixScale',['../d7/d02/classDrawableImage.html#a7582d2267d982d48a9e6ac67dcdc160b',1,'DrawableImage']]],
  ['sethauteur',['setHauteur',['../de/d00/classRectangle.html#a3de1512c544ae2b5d7d64afec49cb4c2',1,'Rectangle']]],
  ['setlargeur',['setLargeur',['../de/d00/classRectangle.html#ab98aa8b3fb04e50302f60c3a468b8c4e',1,'Rectangle']]],
  ['setpoint',['setPoint',['../d2/d51/classTriangle.html#a913ef446985fe71ded74b9663ac01c0b',1,'Triangle']]],
  ['setx',['setX',['../dc/d4f/classPoint.html#a1f963c397e6de87bd14fe54cf3c9c6e0',1,'Point']]],
  ['setxy',['setXY',['../dc/d4f/classPoint.html#a87c3eb0b12f072584b72d9eaff72d9f5',1,'Point::setXY(uint x, uint y)'],['../dc/d4f/classPoint.html#adefae7efa8588db77342cb0a380fb1f6',1,'Point::setXY(istream &amp;is)']]],
  ['sety',['setY',['../dc/d4f/classPoint.html#a3566af0b661dc88cc52f04a25443085c',1,'Point']]]
];
