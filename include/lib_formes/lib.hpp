#ifndef LIB_HPP
#define LIB_HPP

// System :
#include <iostream>
#include <stdexcept>
using namespace std;

// SFML :
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

// Me :
using uint = unsigned int;


#endif /* end of include guard: LIB_HPP */
