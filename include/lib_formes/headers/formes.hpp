#ifndef FORMES_HPP
#define FORMES_HPP

#include "forme.hpp"
#include <cstddef>

/// Permet de manipuler des Formes dans un plan en deux dimensions.
class Formes
{
private:
    size_t _maxFormes;
    size_t _nbFormes;
    Forme ** _formes;

public:
    Formes(size_t = 10);
    Formes(Formes const& src) = delete;

    // Destructeur non-affichée :
    ~Formes();
    // - - - - - - -

    void clean();
    void ajouter(Forme * const);
    Forme & isOver(uint, uint) const;
    friend ostream & operator<<(ostream &, Formes const &);
    void sauver(ostream &) const;
    void charger(istream &);
};

#endif /* end of include guard: FORMES_HPP */
