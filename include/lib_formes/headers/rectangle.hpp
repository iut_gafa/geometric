#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "forme.hpp"

/// Permet de manipuler un Rectangle dans un plan en deux dimensions.
class Rectangle : public virtual Forme
{
private:
    uint _largeur;
    uint _hauteur;
    //void updatePos() override;

protected:
    void ecrire(ostream &) const override;

public:
    /*! Construit un Rectangle à partir de 2 unsigned long int, pour sa taille, et 2 unsigned int, pour son ancre.
     *  @param[in] largeur correspond à la distance de la largeur du Rectangle.
     *  @param[in] hauteur correspond à la distance de la hauteur du Rectangle.
     *  @param[in] x,y est la position du Rectangle dans un plan en deux dimensions.
     */
    Rectangle(ulong largeur, ulong hauteur, uint x, uint y);

    /*! Construit un Rectangle à partir de 2 unsigned long int, pour sa taille, et 2 unsigned int, pour son ancre.
     *  @param[in] largeur correspond à la distance de la largeur du Rectangle.
     *  @param[in] hauteur correspond à la distance de la hauteur du Rectangle.
     *  @param[in, out] ancre correspond au Point d'ancrage du Rectangle.
     */
    Rectangle(ulong largeur, ulong hauteur, Point * const ancre);

    /*! Construit un Rectangle à partir d'un autre Rectangle.*/
    Rectangle(Rectangle & src /** [in, out] est la référence du Rectangle à copier.*/);

    /*! Construit un Rectangle à partir d'un flux.*/
    Rectangle(istream & is /** [in, out] est la référence du flux. */);

    // Destructeur non-affichée :
    ~Rectangle() override;
    // - - - - - - -

    /*! Retourne la distance de la largeur d'un Rectangle. */
    inline uint getLargeur() const { return _largeur; };

    /*! Retourne la distance de la hauteur d'un Rectangle. */
    inline uint getHauteur() const { return _hauteur; };

    /*! Permet de définir la largeur d'un Rectangle.*/
    inline void setLargeur(uint largeur /** [in] correspond à la nouvelle largeur. */) { _largeur = largeur; };

    /*! Permet de définir la hauteur d'un Rectangle.*/
    inline void setHauteur(uint hauteur /** [in] correspond à la nouvelle hauteur. */) { _hauteur = hauteur; };

    /*! Renvoie le périmètre d'un Rectangle.*/
    double perimetre() const override;

    /*! Renvoie si une position se situe à l'intérieur d'un Rectangle.
     *  @param[in] x,y Position d'un Point dans un plan en deux dimensions.
    */
    bool isOver(uint, uint) const override;
};

#endif /* end of include guard: RECTANGLE_HPP */
