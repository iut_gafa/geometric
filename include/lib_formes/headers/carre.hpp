#ifndef CARRE_HPP
#define CARRE_HPP

#include "forme.hpp"

/// Permet de manipuler un Carre dans un plan en deux dimensions.
class Carre : virtual public Forme
{
private:
    uint _cote;
    //void updatePos() override;

protected:
    void ecrire(ostream &) const override;

public:
    Carre(ulong, uint, uint);
    Carre(ulong, Point *);
    Carre(Carre const&);
    Carre(istream &);
    ~Carre() override;
    inline uint getCote() const { return _cote; };
    inline void setCote(uint cote) { _cote = cote; };
    double perimetre() const override;
    bool isOver(uint, uint) const override;
};

#endif /* end of include guard: CARRE_HPP */
