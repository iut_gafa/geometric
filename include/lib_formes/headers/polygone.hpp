#ifndef POLYGONE_HPP
#define POLYGONE_HPP

#include "forme.hpp"
#include <iostream>
#include <vector>
#include <cmath>
#include <climits>

/// Permet de manipuler un Polygone dans un plan en deux dimensions.
class Polygone : public virtual Forme
{
private:
    std::vector<Point> * _points;
    std::vector<double> * _sideLenght;

    void defineSquare();

protected:
    void ecrire(ostream & os) const override;
    Point _min, _max;

public:
    /*! Construit un Polygone à partir d'un std::vector de Point et 2 unsigned int pour l'ancre.
     *  @param[in, out] pone correspond au Premier Point du Triangle.
     *  @param[in, out] ptwo correspond au Premier Point du Triangle.
     *  @param[in, out] pthree correspond au Premier Point du Triangle.
     *  @param[in] x,y est la position du Triangle dans un plan en deux dimensions.
     */
    Polygone(std::vector<Point> const & points, uint x, uint y);
    Polygone(std::vector<Point> const & points, Point * const ancre);
    Polygone(Polygone const & src);
    Polygone(istream & is);

    // Méthodes non-affichées :
    ~Polygone() override;
    // - - - - - - -

    void calculateSide();
    double perimetre() const override;
    bool isOver(uint x, uint y) const override;

    inline uint getSize() const {return _points->size();};
    inline std::vector<Point> const * const getAllPoints() const {return _points;};
    Point & getPoint(uint at) const;
    void setPoint(uint at, Point & point);
};

#endif /* end of include guard: POLYGONE_HPP */
