#ifndef FORME_HPP
#define FORME_HPP
// Me :
#include "point.hpp"

// System :
#include <iostream>
#include <stdexcept>

/// Représente une Forme dans un plan en deux dimensions.
class Forme
{
private:
    Point * _ancre;
    bool _fromPoint;
    //virtual void updatePos() = 0;
    void deleteAncre();

protected:
    virtual void ecrire(std::ostream &) const;

public:
    //Constructeur-----------------------------------------
    Forme(Point * const ancre);
    Forme(uint, uint);
    Forme(Forme const &);
    Forme(std::istream & is);
    Forme() = delete;

    // Destructeur non-affichée :
    virtual ~Forme();
    // - - - - - - -


    inline Point * getAncre() const { return (_ancre); };
    inline virtual void setAncre(uint x, uint y) { _ancre->setXY(x, y);};
    virtual void setAncre(Point * const ancre);

    virtual double perimetre() const = 0;
    virtual bool isOver(uint, uint) const = 0;
    friend std::ostream & operator<<(std::ostream &, Forme const &);
    static Forme * charger(std::istream &);

};

#endif /* end of include guard: FORME_HPP */
