#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "forme.hpp"
#include <iostream>
#include <cmath>

/// Permet de manipuler un Triangle dans un plan en deux dimensions.
class Triangle : public virtual Forme
{
private:
    Point * _points[3];
    float _sideLenght[3];
    double dot(Point &, Point &) const;
    bool _byIs[3];
    void calculateSide();

protected:
    void ecrire(ostream &) const override;

public:
    /*! Construit un Triangle à partir de 3 Point et 2 unsigned int pour l'ancre.
     *  @param[in, out] pone correspond au Premier Point du Triangle.
     *  @param[in, out] ptwo correspond au Premier Point du Triangle.
     *  @param[in, out] pthree correspond au Premier Point du Triangle.
     *  @param[in] x,y est la position du Triangle dans un plan en deux dimensions.
     */
    Triangle(Point * const pone, Point * const ptwo, Point * const pthree, uint x, uint y);

    /*! Construit un Triangle à partir de 3 Point et 2 unsigned int pour l'ancre.
     *  @param[in, out] pone correspond au premier Point du Triangle.
     *  @param[in, out] ptwo correspond au deuxième Point du Triangle.
     *  @param[in, out] pthree correspond au troisième Point du Triangle.
     *  @param[in, out] ancre correspond au Point d'ancrage du Triangle.
     */
    Triangle(Point * const pone, Point * const ptwo, Point * const pthree, Point * const ancre);

    /*! Construit un Triangle à partir d'un autre Triangle.*/
    Triangle(Triangle const & src /** [in, out] est la référence du Triangle à copier.*/);

    /*! Construit un Triangle à partir d'un flux.*/
    Triangle(istream & is /** [in, out] est la référence du flux. */);

    // Destructeur non-affichée :
    ~Triangle() override;
    // - - - - - - -

    /*! Renvoie un pointeur constant sur un des trois Point d'un Triangle.*/
    Point * const getPoint(uint numPoint /** [in, out] numéro du point à retourner. */) const;

    /*! Permet de modifier un des trois Point d'un Triangle.
     *  @param[in] numPoint correspond au numéro du Point à modifier.
     *  @param[in, out] point correspond au nouveau Point.
     */
    void setPoint(uint numPoint, Point * const point);

    /*! Renvoie le périmètre d'un Triangle.*/
    double perimetre() const override;

    /*! Renvoie si une position se situe à l'intérieur d'un Triangle.
     *  @param[in] x,y Position du Point dans un plan en deux dimensions.
    */
    bool isOver(uint x, uint y) const override;
};

#endif /* end of include guard: TRIANGLE_HPP */
