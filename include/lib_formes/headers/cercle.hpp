#ifndef CERCLE_HPP
#define CERCLE_HPP

#include "forme.hpp"

/// Permet de manipuler un Cercle dans un plan en deux dimensions.
class Cercle : public virtual Forme
{
private:
    //void updatePos() override;
    uint _rayon;

protected:
    void ecrire(ostream &) const override;

public:
    Cercle(ulong, uint, uint);
    Cercle(ulong, Point * const p);
    Cercle(Cercle &);
    Cercle(istream &);
    ~Cercle() override;
    inline uint getRayon() const { return _rayon; };
    inline void setRayon(uint rayon) { _rayon = rayon; };
    double perimetre() const override;
    bool isOver(uint, uint) const override;
};

#endif /* end of include guard: CERCLE_HPP */
