#ifndef POINT_HPP
#define POINT_HPP

// System :
#include <iostream>
#include <stdexcept>
using namespace std;

/// Permet de manipuler un Point dans un plan en deux dimensions.
class Point
{

private:
    uint const _taille = 3;
    uint _x, _y;
    void updatePos();

public:
    /*! Construit un Point à partir de 2 unsigned int.
     *  @param[in] x,y Position du Point dans un plan en deux dimensions.
     */
    Point(uint x, uint y);

    /*! Construit un Point à partir d'un autre Point. */
    Point(Point const & src /** [in, out] est la référence du Point à copier.*/);

    /*! Construit un Point à partir d'un flux. */
    Point(istream & is /** [in, out] est la référence du flux. */);

    // Méthodes non-affichées :
    Point() = delete;
    ~Point();
    // - - - - - - -


    /*! Retourne la valeur de la position x d'un Point. */
    inline uint getX() const {return _x;};

    /*! Retourne la valeur de la position y d'un Point. */
    inline uint getY() const {return _y; };

    /*! Permet de définir la position x d'un Point. */
    inline void setX(uint x) {_x = x;};

    /*! Permet de définir la position y d'un Point. */
    inline void setY(uint y) {_y = y;};

    /*! Permet de définir la position d'un Point.
     *  @param[in] x,y Position du Point dans un plan en deux dimensions.
     */
    void setXY(uint x, uint y);

    /*! Permet de définir la position d'un Point à partir d'un flux.*/
    void setXY(istream & is /** [in, out] est la référence du flux. */);

    /*! Renvoie si une position se situe au dessus d'un Point ou non.
     *  @param[in] x,y Position du Point dans un plan en deux dimensions.
     */
    inline bool isOver(uint x, uint y) const { return ((x>=_x?x-_x:_x-x)<=_taille && (y>=_y?y-_y:_y-y)<=_taille); };

    /*! operateur d'assignation pour un Point.
     *  @param[in, out] src est la référence du Point référent.
     */
    Point & operator=(Point const & src);

    /*! Permet d'injecter les informations d'un Point dans un flux.*
     *  @param[in, out] os est la référence du flux, où sont injectées les informations.
     *  @param[in, out] src est la référence du Point, où les informations vont être assignées.
     */
    friend ostream & operator<<(ostream & os, Point const & src);

    /*! Permet d'extraire les informations d'un flux dans un Point.
    *  @param[in, out] is est la référence du flux, où sont extraites les informations.
    *  @param[in, out] src est la référence du Point, où les informations sont assignées.
    */
    friend istream & operator>>(istream & is, Point & src);

};

#endif /* end of include guard: POINT_HPP */
