#ifndef ELLIPSE_HPP
#define ELLIPSE_HPP

#include "forme.hpp"

/// Permet de manipuler une Ellipse dans un plan en deux dimensions.
class Ellipse : public virtual Forme
{
private:
    uint _demiLargeur;
    uint _demiHauteur;
    //void updatePos() override;

protected:
    void ecrire(ostream &) const override;

public:
    Ellipse(ulong, ulong, uint, uint);
    Ellipse(ulong, ulong, Point * const p);
    Ellipse(Ellipse &);
    Ellipse(istream &);
    ~Ellipse() override;
    inline uint getDemiLargeur() const { return _demiLargeur; };
    inline uint getDemiHauteur() const { return _demiHauteur; };
    inline void setDemiLargeur(uint demiLargeur) { _demiLargeur = demiLargeur; };
    inline void setDemiHauteur(uint demiHauteur) { _demiHauteur = demiHauteur; };
    double perimetre() const override;
    bool isOver(uint, uint) const override;
};

#endif /* end of include guard: ELLIPSE_HPP */
