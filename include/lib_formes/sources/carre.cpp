#include "../headers/carre.hpp"

Carre::Carre(ulong cote, uint x, uint y) : Forme(x, y), _cote(cote)
{}

Carre::Carre(ulong cote, Point * const ancre) : Forme(ancre), _cote(cote)
{}

Carre::Carre(Carre const& src) : Carre(src._cote, src.getAncre()->getX(), src.getAncre()->getY())
{}

Carre::Carre(istream & is) : Forme(is)
{
    is >> _cote;
}


Carre::~Carre()
{}

bool Carre::isOver(uint x, uint y) const
{
    return ( x >= getAncre()->getX() && x <= getAncre()->getX() + _cote && y >= getAncre()->getY() && y <= getAncre()->getY() + _cote);
}

double Carre::perimetre() const
{
    return _cote * 2;
}

void Carre::ecrire(ostream & os) const
{
    os << "Carre ";
    Forme::ecrire(os);
    os << " " << _cote;
}
