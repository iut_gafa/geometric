#include "../headers/formes.hpp"

Formes::Formes(size_t taille) : _maxFormes(taille), _nbFormes(0), _formes(nullptr)
{
    _formes = new Forme * [_maxFormes];
}

Formes::~Formes()
{
    clean();
}

void Formes::clean()
{
    for (size_t i = 0; i < _nbFormes; i++)
    {
        delete (_formes[i]);
    }
    delete [] _formes;
}

void Formes::ajouter(Forme * const src)
{
    if (_nbFormes >= _maxFormes)
        throw runtime_error("Plus de place");
    else
        _formes[_nbFormes++] = src;
}

Forme & Formes::isOver(uint x, uint y) const
{
    for (size_t i = 0; i < _nbFormes; i++)
    {
        if (_formes[i]->isOver(x,y))
            return *(_formes[i]);
    }

    throw invalid_argument("Pas de Forme ici");
}

ostream & operator<<(ostream  & os, Formes const & fs)
{
    fs.sauver(os);
    return os;
}

void Formes::sauver(ostream & os ) const
{
    os << _maxFormes << " " << _nbFormes << endl;
    for (size_t i = 0; i < _nbFormes; i++)
        os << *(_formes[i]) << endl;
}

void Formes::charger(istream & is)
{
    clean();
    is >> _maxFormes;
    _formes = new Forme * [_maxFormes];
    size_t combien;
    is >> combien;
    for (size_t i = 0; i < combien; i++)
        ajouter(Forme::charger(is));
}
