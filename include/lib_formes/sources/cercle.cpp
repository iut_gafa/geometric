#include "../headers/cercle.hpp"

const double pi = 3.14159265358979323846264;

Cercle::Cercle(ulong rayon, uint x, uint y) : Forme(x, y), _rayon(rayon)
{}

Cercle::Cercle(ulong rayon, Point * const ancre) : Forme(ancre), _rayon(rayon)
{}

Cercle::Cercle(Cercle & src) : Cercle(src._rayon, src.getAncre()->getX(), src.getAncre()->getY())
{}

Cercle::Cercle(istream & is) : Forme(is)
{
    is >> _rayon;
}


Cercle::~Cercle()
{}

bool Cercle::isOver(uint x, uint y) const
{
    return ( x >= getAncre()->getX() && x <= getAncre()->getX() + _rayon*2 && y >= getAncre()->getY() && y <= getAncre()->getY() + _rayon*2);
}

double Cercle::perimetre() const
{
    return 2 * pi *_rayon;
}

void Cercle::ecrire(ostream & os) const
{
    os << "Cercle ";
    Forme::ecrire(os);
    os << " " << _rayon;
}
