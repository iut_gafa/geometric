#include "../headers/rectangle.hpp"

Rectangle::Rectangle(ulong largeur, ulong hauteur, uint x, uint y) : Forme(x, y), _largeur(largeur), _hauteur(hauteur)
{
}

Rectangle::Rectangle(ulong largeur, ulong hauteur, Point * const ancre) : Forme(ancre), _largeur(largeur), _hauteur(hauteur)
{
}

Rectangle::Rectangle(Rectangle & src) : Rectangle(src._largeur, src._hauteur, src.getAncre()->getX(), src.getAncre()->getY())
{
}

Rectangle::Rectangle(istream & is) : Forme(is)
{
    is >> _largeur;
    is >> _hauteur;
}


Rectangle::~Rectangle()
{
}

bool Rectangle::isOver(uint x, uint y) const
{
    return ( x >= getAncre()->getX() && x <= getAncre()->getX() + _largeur && y >= getAncre()->getY() && y <= getAncre()->getY() + _hauteur);
}

double Rectangle::perimetre() const
{
    return (_largeur + _hauteur) * 2;
}

void Rectangle::ecrire(ostream & os) const
{
    os << "Rectangle ";
    Forme::ecrire(os);
    os << " " << _largeur << " " << _hauteur;
}
