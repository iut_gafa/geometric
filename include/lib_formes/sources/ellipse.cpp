#include "../headers/ellipse.hpp"
#include <cmath>

const double pi = 3.14159265358979323846264;

Ellipse::Ellipse(ulong demiLargeur, ulong demiHauteur, uint x, uint y) : Forme(x, y), _demiLargeur(demiLargeur), _demiHauteur(demiHauteur)
{}

Ellipse::Ellipse(ulong demiLargeur, ulong demiHauteur, Point * const ancre) : Forme(ancre), _demiLargeur(demiLargeur), _demiHauteur(demiHauteur)
{}

Ellipse::Ellipse(Ellipse & src) : Ellipse(src._demiLargeur, src._demiHauteur, src.getAncre()->getX(), src.getAncre()->getY())
{}

Ellipse::Ellipse(istream & is) : Forme(is)
{
    is >> _demiLargeur;
    is >> _demiHauteur;
}


Ellipse::~Ellipse()
{}

bool Ellipse::isOver(uint x, uint y) const
{
    return ( x >= getAncre()->getX() && x <= getAncre()->getX() + _demiLargeur*2 && y >= getAncre()->getY() && y <= getAncre()->getY() + _demiHauteur*2);
}

double Ellipse::perimetre() const
{
    uint a;
    uint b;
    if (_demiLargeur > _demiHauteur)
    {
        a = _demiLargeur;
        b = _demiHauteur;
    }
    else
    {
        a = _demiHauteur;
        b = _demiLargeur;
    }

    double h = ((a - b) * (a - b)) / ((a + b) * (a + b));

    return pi * (a + b) * (1 + ((3 * h) / (10 + sqrt(4 - 3 * h))));
}

void Ellipse::ecrire(ostream & os) const
{
    os << "Ellipse ";
    Forme::ecrire(os);
    os << " " << _demiLargeur << " " << _demiHauteur;
}
