#include "../headers/point.hpp"

Point::Point(uint x, uint y) : _x(x), _y(y) {}

Point::Point(Point const & src) : Point(src._x, src._y) {}

Point::Point(istream & is)
{
    is >> _x;
    is >> _y;
}

Point & Point::operator=(Point const & src)
{
    _x = src._x;
		_y = src._y;
    return *(this);
}

Point::~Point()
{
}

void Point::setXY(uint x, uint y)
{
    _x = x;
    _y = y;
}

void Point::setXY(istream & is)
{
    is >> _x;
    is >> _y;
}

ostream & operator<<(ostream & os, Point const & src)
{
    os << src.getX() << " " << src.getY();
    return os;
}

istream & operator>>(istream & is, Point & src)
{
    src.setXY(is);
    return is;
}
