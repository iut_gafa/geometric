#include "../headers/polygone.hpp"

Polygone::Polygone(std::vector<Point> const & points, uint x, uint y)
: Forme(x, y), _points(new std::vector<Point>(points)),
_sideLenght(new std::vector<double>(_points->size())),
_min(Point(UINT_MAX, UINT_MAX)), _max(Point(0, 0))
{
    calculateSide();
    defineSquare();
}


Polygone::Polygone(std::vector<Point> const & points, Point * const ancre)
: Forme(ancre), _points(new std::vector<Point>(points)),
_sideLenght(new std::vector<double>(_points->size())),
_min(Point(UINT_MAX, UINT_MAX)), _max(Point(0, 0))
{
    calculateSide();
    defineSquare();
}


Polygone::Polygone(Polygone const & src) : Polygone(*src._points, src.getAncre()->getX(), src.getAncre()->getY())
{

}


Polygone::Polygone(istream & is) : Forme(is), _min(Point(UINT_MAX, UINT_MAX)), _max(Point(0, 0))
{
    uint size;
    is >> size;

    _points = new std::vector<Point>();

    _sideLenght = new std::vector<double>();

    for (size_t i = 0; i < size; i++)
    {

        _points->push_back(Point(is));
    }

    calculateSide();
    defineSquare();
}


Polygone::~Polygone()
{
    delete _sideLenght;
    delete _points;
}

void Polygone::calculateSide()
{
    _sideLenght->clear();
    _sideLenght->reserve(_points->size());


    for (size_t i = 0; i < _points->size()-1; i++)
    {
        _sideLenght->push_back(
            std::sqrt(
                std::pow(int(_points->at(i+1).getX() - _points->at(i).getX()), 2)
                +
                std::pow(int(_points->at(i+1).getY() - _points->at(i).getY()), 2)));
    }

    _sideLenght->push_back(
        std::sqrt(
            std::pow(int(_points->at(0).getX() - _points->at(_sideLenght->size()).getX()), 2)
            +
            std::pow(int(_points->at(0).getY() - _points->at(_sideLenght->size()).getY()), 2)));

}

double Polygone::perimetre() const
{
    double tmpsize = 0;

    for (size_t i = 0; i < _sideLenght->size(); i++)
    {
        tmpsize += _sideLenght->at(i);
    }

    return tmpsize;
}

void Polygone::defineSquare()
{

    for (size_t i = 0; i < _points->size(); i++)
    {
        if (_points->at(i).getX() > _max.getX())
            _max.setX(_points->at(i).getX());
        if (_points->at(i).getY() > _max.getY())
            _max.setY(_points->at(i).getY());
        if (_points->at(i).getX() < _min.getX())
            _min.setX(_points->at(i).getX());
        if (_points->at(i).getY() < _min.getY())
            _min.setY(_points->at(i).getY());
    }
}

bool Polygone::isOver(uint x, uint y) const
{
    return x <=_max.getX() && x >=_min.getX() && y <=_max.getY() && y >=_min.getY();
}

void Polygone::ecrire(ostream & os) const
{
    os << "Polygone ";
    Forme::ecrire(os);

    // nb de points :
    os << " " << _points->size();

    // points :
    for (size_t i = 0; i < _points->size(); i++)
        os << " " << _points->at(i);
}

Point & Polygone::getPoint(uint at) const
{
    try
    {
        if (at >= _points->size())
            throw at;
    }
    catch (uint amount)
    {
        std::cerr << std::endl << "Erreur : L'argument at = '" << amount << "' est invalide."
        << std::endl << "-> Ce Polygone ne possède que " << _points->size()<< " côtés.";
    }

    return _points->at(at);
}

void Polygone::setPoint(uint at, Point & point)
{
    try
    {
        _points->at(at) = point;
    }
    catch (uint amount)
    {
        std::cerr << std::endl << "Erreur : L'argument at = '" << amount << "' est invalide."
        << std::endl << "-> Ce Polygone ne possède que " << _points->size()<< " côtés.";
    }
}
