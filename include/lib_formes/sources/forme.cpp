#include "../headers/forme.hpp"
#include "../headers/rectangle.hpp"
#include "../headers/carre.hpp"
#include "../headers/ellipse.hpp"
#include "../headers/cercle.hpp"

Forme::Forme(uint x, uint y) : _ancre(new Point(x, y)), _fromPoint(false)
{
    // _ancre needs to be deleted
}

Forme::Forme(Point * const ancre) : _ancre(ancre), _fromPoint(true)
{
}

Forme::Forme(Forme const & src) : _ancre(new Point(_ancre->getX(), _ancre->getY())), _fromPoint(true)
{
}

Forme::Forme(istream & is) : _ancre(new Point(is)), _fromPoint(false)
{
}

Forme::~Forme()
{
    if (!_fromPoint)
    {
        delete _ancre;
    }
}

void Forme::setAncre(Point * const ancre)
{
    // Si Forme a précédement créé un point, alors on le supprimme :
    if (!_fromPoint)
    {
        delete _ancre;
    }

    // Forme change la cible
    _ancre = ancre;

    _fromPoint = true; // Forme n'est plus responsable de la destruction du point
}

void Forme::ecrire(ostream & os) const
{
    os << *(_ancre);
}

Forme * Forme::charger(istream & is)
{
    string type;

    is >> type;

    if (type == "Rectangle")
        return new Rectangle(is);
    if (type == "Carre")
        return new Carre(is);
    else if (type == "Ellipse")
        return new Ellipse(is);
    else if (type == "Cercle")
        return new Cercle(is);
    else
        throw out_of_range("Forme non prise en compte");

}

ostream & operator<<(ostream & os, Forme const & f)
{
    f.ecrire(os);
    return os;
}
