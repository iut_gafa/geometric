#include "../headers/triangle.hpp"

Triangle::Triangle(Point * const pone, Point * const ptwo, Point * const pthree, uint x, uint y)
:  Forme(x, y), _points{pone, ptwo, pthree}, _byIs{false, false, false}
{
    calculateSide();
}

Triangle::Triangle(Point * const pone, Point * const ptwo, Point * const pthree, Point * const ancre)
: Forme(ancre), _points{pone, ptwo, pthree}, _byIs{false, false, false}
{
    calculateSide();
}

Triangle::Triangle(Triangle const & src)
: Triangle(src._points[0], src._points[1], src._points[2], src.getAncre()->getX(), src.getAncre()->getY())
{

}

Triangle::Triangle(istream & is) : Forme(is), _byIs{true, true, true}
{
    uint tmpx, tmpy;
    is >> tmpx;
    is >> tmpy;
    _points[0] = new Point(tmpx, tmpy);
    is >> tmpx;
    is >> tmpy;
    _points[1] = new Point(tmpx, tmpy);
    is >> tmpx;
    is >> tmpy;
    _points[2] = new Point(tmpx, tmpy);

    calculateSide();
}

Triangle::~Triangle()
{
    // Si ce Triangle a été créer à partir d'un istream,
    // alors on doit delete nos points
    if (_byIs[0])
        delete _points[0];
    if (_byIs[1])
        delete _points[1];
    if (_byIs[2])
        delete _points[2];
}

void Triangle::calculateSide()
{
    _sideLenght[0] = std::sqrt(std::pow(int(_points[1]->getX() - _points[0]->getX()), 2) + std::pow(int(_points[1]->getY() - _points[0]->getY()), 2));
    _sideLenght[1] = std::sqrt(std::pow(int(_points[2]->getX() - _points[1]->getX()), 2) + std::pow(int(_points[2]->getY() - _points[1]->getY()), 2));
    _sideLenght[2] = std::sqrt(std::pow(int(_points[0]->getX() - _points[2]->getX()), 2) + std::pow(int(_points[0]->getY() - _points[2]->getY()), 2));
}

double Triangle::perimetre() const
{

    return _sideLenght[0] + _sideLenght[1] + _sideLenght[2];
}

double Triangle::dot(Point & a, Point & b) const
{
    return a.getX() * b.getX() + a.getY() * b.getY();
}

bool Triangle::isOver(uint x, uint y) const
{
    // http://blackpawn.com/texts/pointinpoly/default.html # for more informations

    // Compute vectors :
    Point v0 = Point(getAncre()->getX() + _points[2]->getX() - getAncre()->getX() + _points[0]->getX(), getAncre()->getY() + _points[2]->getY() - getAncre()->getY() + _points[0]->getY());
    Point v1 = Point(getAncre()->getX() + _points[1]->getX() - getAncre()->getX() + _points[0]->getX(), getAncre()->getY() + _points[1]->getY() -  getAncre()->getY() +_points[0]->getY());
    Point v2 = Point(x - getAncre()->getX() + _points[0]->getX(), y - getAncre()->getY() + _points[0]->getY());

    // Compute dot products :
    double dot00 = dot(v0, v0);
    double dot01 = dot(v0, v1);
    double dot02 = dot(v0, v2);
    double dot11 = dot(v1, v1);
    double dot12 = dot(v1, v2);

    // Compute barycentric coordinates :
    double invDenom = 1 / double(dot00 * dot11 - dot01 * dot01);
    double u = (dot11 * dot02 - dot01 * dot12) * invDenom;
    double v = (dot00 * dot12 - dot01 * dot02) * invDenom;

    // Check if point is in triangle
    return (u >= 0) && (v >= 0) && (u + v < 1);
}

Point * const Triangle::getPoint(uint numPoint) const
{
    try
    {
        if (numPoint > 2)
            throw numPoint;
    }
    catch (uint amount)
    {
        std::cerr << std::endl << "Erreur : L'argument numPoint = " << amount << " est invalide."
        << std::endl << "-> Un triangle ne possède que trois côtés. Les arguments valident sont : '0', '1' et '2'.";
    }

    return _points[numPoint];
}

void Triangle::setPoint(uint numPoint, Point * const newPoint)
{
    try
    {
        if (numPoint > 2)
            throw numPoint;
    }
    catch (uint amount)
    {
        std::cerr << std::endl << "Erreur : L'argument numPoint = '" << amount << "' est invalide."
        << std::endl << "-> Un triangle ne possède que trois côtés. Les arguments valident sont : '0', '1' et '2'.";
    }

    if (_byIs[numPoint])
        delete _points[numPoint];

    _points[numPoint] = newPoint;
}

void Triangle::ecrire(ostream & os) const
{
    os << "Triangle ";
    Forme::ecrire(os);
    os << " " << *_points[0] << " " << *_points[1] << " " << *_points[2];
}
