#include "../headers/DrawableEllipsis.hpp"

DrawableEllipsis::DrawableEllipsis(sf::Color const & couleur, ulong demiLargeur, ulong demiHauteur, uint x, uint y)
: Forme(x, y),
Drawable(x, y, new sf::CircleShape(), couleur),
Ellipse(demiLargeur, demiHauteur, x, y)
{
    defineShape();
}

DrawableEllipsis::DrawableEllipsis(sf::Color const & couleur, ulong demiLargeur, ulong demiHauteur, Point * const ancre)
: Forme(ancre),
Drawable(ancre, new sf::CircleShape(), couleur),
Ellipse(demiLargeur, demiHauteur, ancre)
{
    defineShape();
}

DrawableEllipsis::DrawableEllipsis(DrawableEllipsis const & src)
: DrawableEllipsis(src.getColor(), src.getDemiLargeur(), src.getDemiHauteur(), src.getAncre()->getX(), src.getAncre()->getY())
{

}

DrawableEllipsis::DrawableEllipsis(std::istream& is)
: Forme(is), Drawable(getAncre()->getX(), getAncre()->getY(), new sf::CircleShape()), Ellipse(is) {
  ulong color;
  is >> color;
  setColor(sf::Color(color));
  defineShape();
}

DrawableEllipsis::~DrawableEllipsis()
{

}

void DrawableEllipsis::defineShape()
{
    sf::CircleShape * cShape = (sf::CircleShape *) _shape; // Dynamique Cast pour définir le cercle

    // Définition
    cShape->setPointCount(90);
}

void DrawableEllipsis::updateShape()
{
    sf::CircleShape * cShape = (sf::CircleShape *) _shape; // Dynamique Cast pour définir le cercle

    if (getDemiLargeur() > getDemiHauteur())
    {
        cShape->setRadius(getDemiHauteur());
        cShape->setScale(getDemiLargeur()/getDemiHauteur(), 1);
    }
    else
    {
        cShape->setRadius(getDemiLargeur());
        cShape->setScale(1, getDemiHauteur()/getDemiLargeur());
    }

    Drawable::updateShape(); // On appelle la méthode mère
}

void DrawableEllipsis::ecrire(std::ostream& os) const {
  os << "DrawableEllipsis ";
  Ellipse::ecrire(os);
  Drawable::ecrire(os);
}

void DrawableEllipsis::moveTo(sf::Vector2i coord) {
	setAncre(coord.x - getDemiLargeur(), coord.y - getDemiHauteur());
}
