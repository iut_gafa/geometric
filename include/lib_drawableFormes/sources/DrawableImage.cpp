#include "../headers/DrawableImage.hpp"

DrawableImage::DrawableImage(const std::string & filename, uint width, uint height, uint x, uint y)
: Forme(x, y),
Drawable(x, y, new sf::RectangleShape, sf::Color::White),
_texture(new sf::Texture()), _height(height), _width(width), _filename(filename),
_fixScale(false), _scale(double(_height)/double(_width))
{
    defineShape();
}

DrawableImage::DrawableImage(const std::string & filename, uint width, uint height, Point * const ancre)
: Forme(ancre),
Drawable(ancre, new sf::RectangleShape, sf::Color::White),
_texture(new sf::Texture()), _height(height), _width(width), _filename(filename),
_fixScale(false), _scale(double(_height)/double(_width))
{
    defineShape();
}

DrawableImage::DrawableImage(DrawableImage const & src)
: DrawableImage(src._filename, src._height, src._width, src.getAncre()->getX(), src.getAncre()->getY())
{

}

DrawableImage::DrawableImage(std::istream& is) :
Forme(is),
Drawable(getAncre()->getX(), getAncre()->getY(), new sf::RectangleShape(),sf::Color::White),
_texture(new sf::Texture()) {
	is >> _width;
	is >> _height;
	is >> _filename;
	_scale = (double(_height)/double(_width));

	defineShape();
}

DrawableImage::~DrawableImage()
{

}

void DrawableImage::defineShape()
{
	//On charge l'image
	_texture->loadFromFile(_filename);

    // Shape :

    sf::RectangleShape * rShape = (sf::RectangleShape *) _shape; // Dynamique Cast pour définir le cercle

    rShape->setTexture(_texture, true);
    rShape->setSize(sf::Vector2f(_width, _height));

}

void DrawableImage::updateShape()
{
	    // Shape :

    sf::RectangleShape * rShape = (sf::RectangleShape *) _shape; // Dynamique Cast pour définir le cercle

    rShape->setSize(sf::Vector2f(_width, _height));
    Drawable::updateShape(); // On appelle la méthode mère
}

double DrawableImage::perimetre() const
{
    return _height * _width;
}

bool DrawableImage::isOver(uint x, uint y) const
{
    return ( x >= getAncre()->getX() && x <= getAncre()->getX() + _height && y >= getAncre()->getY() && y <= getAncre()->getY() + _width);
}

void DrawableImage::setNewScale()
{
    _scale = double(_height)/double(_width);
}

void DrawableImage::setHeight(uint size)
{
    _height = size;

    if (_fixScale)
        _width = _height * 1/_scale;
    else
        setNewScale();
}

void DrawableImage::setWidth(uint size)
{
    _width = size;

    if (_fixScale)
        _height = _width * _scale;
    else
        setNewScale();
}

void DrawableImage::setAlpha(sf::Uint8 amount)
{
    sf::Color tmpColor = getColor();
    tmpColor.a = amount;
    setColor(tmpColor);
}

void DrawableImage::moveTo(sf::Vector2i coord) {
	setAncre(coord.x - getWidth()/2, coord.y - getHeight()/2);
}

void DrawableImage::ecrire(std::ostream& os) const {
	os << "DrawableImage ";
	Forme::ecrire(os);
	os << " " <<_width << " " << _height << " " << _filename;
}
