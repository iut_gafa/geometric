#include "../headers/DrawableCircle.hpp"

DrawableCircle::DrawableCircle(sf::Color const & couleur, ulong rayon, uint x, uint y)
: Forme(x, y),
Drawable(x, y, new sf::CircleShape(), couleur),
Cercle(rayon, x, y)
{
    defineShape();
}

DrawableCircle::DrawableCircle(sf::Color const & couleur, ulong rayon, Point * const ancre)
: Forme(ancre),
Drawable(ancre, new sf::CircleShape(), couleur),
Cercle(rayon, ancre)
{
    defineShape();
}

DrawableCircle::DrawableCircle(DrawableCircle const & src)
: DrawableCircle(src.getColor(), src.getRayon(), src.getAncre()->getX(), src.getAncre()->getY())
{

}

DrawableCircle::DrawableCircle(std::istream& is)
: Forme(is), Drawable(getAncre()->getX(), getAncre()->getY(), new sf::CircleShape()), Cercle(is) {
  ulong color;
  is >> color;
  setColor(sf::Color(color));
  defineShape();
}


DrawableCircle::~DrawableCircle()
{

}

void DrawableCircle::defineShape()
{
    sf::CircleShape * cShape = (sf::CircleShape *) _shape; // Dynamique Cast pour définir le cercle

    // Définition
    cShape->setPointCount(90);
}

void DrawableCircle::updateShape()
{
    sf::CircleShape * cShape = (sf::CircleShape *) _shape; // Dynamique Cast pour définir le cercle

    //On vérifie si ces propriétés n'ont pas changé
    cShape->setRadius(getRayon());
    Drawable::updateShape(); // On appelle la méthode mère
}

void DrawableCircle::ecrire(std::ostream& os) const {
  os << "DrawableCircle ";
  Cercle::ecrire(os);
  Drawable::ecrire(os);
}

void DrawableCircle::moveTo(sf::Vector2i coord) {
	setAncre(coord.x - getRayon(), coord.y - getRayon());
}
