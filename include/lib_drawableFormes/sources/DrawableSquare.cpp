#include "../headers/DrawableSquare.hpp"

DrawableSquare::DrawableSquare(sf::Color const& c, ulong cote, uint x, uint y)
  : Forme(x, y), Drawable(x, y, new sf::RectangleShape(), c), Carre(cote, x, y) {
    defineShape();
}

DrawableSquare::DrawableSquare(sf::Color const& c, ulong cote, Point * const ancre)
  : Forme(ancre), Drawable(ancre, new sf::RectangleShape(), c), Carre(cote, ancre) {
    defineShape();
  }

DrawableSquare::DrawableSquare(DrawableSquare const& src)
  : DrawableSquare(getColor(), getCote(), getAncre()->getX(), getAncre()->getY()) {}

  DrawableSquare::DrawableSquare(std::istream& is)
  : Forme(is), Drawable(getAncre()->getX(), getAncre()->getY(), new sf::RectangleShape()), Carre(is) {
    ulong color;
    is >> color;
    setColor(sf::Color(color));
    defineShape();
  }

DrawableSquare::~DrawableSquare()
{

}

void DrawableSquare::defineShape() {

}

void DrawableSquare::updateShape() {
  sf::RectangleShape* rShape = (sf::RectangleShape*) _shape; //Il nous faut faire un dynamic cast
	//Actualise pour toujours dessiner correctement.
  rShape -> setSize(sf::Vector2f(getCote(), getCote())); //Pour appeler setSize() propre à un rectangle
  Drawable::updateShape(); //Finalement on appel la fonction commune
}

void DrawableSquare::ecrire(std::ostream& os) const {
  os << "DrawableSquare ";
  Carre::ecrire(os);
  Drawable::ecrire(os);
}

void DrawableSquare::moveTo(sf::Vector2i coord) {
	setAncre(coord.x - getCote()/2, coord.y - getCote()/2);
}
