#include "../headers/DrawableTriangle.hpp"

DrawableTriangle::DrawableTriangle(sf::Color const & couleur, Point * const pone, Point * const ptwo, Point * const pthree, uint x, uint y)
: Forme(x, y),
Drawable(x, y, new sf::ConvexShape(), couleur),
Triangle(pone, ptwo, pthree, x, y)
{
    defineShape();
}

DrawableTriangle::DrawableTriangle(sf::Color const & couleur, Point * const pone, Point * const ptwo, Point * const pthree, Point * const ancre)
: Forme(ancre),
Drawable(ancre, new sf::ConvexShape(), couleur),
Triangle(pone, ptwo, pthree, ancre)
{
    defineShape();
}

DrawableTriangle::DrawableTriangle(DrawableTriangle const & src)
: DrawableTriangle(src.getColor(), src.getPoint(0), src.getPoint(1), src.getPoint(2), src.getAncre())
{
    // Pas de defineShape()
}


DrawableTriangle::DrawableTriangle(std::istream & is)
: Forme(is), Drawable(getAncre()->getX(), getAncre()->getY(), new sf::ConvexShape()), Triangle(is)
{
    ulong color;
    is >> color;
    setColor(sf::Color(color));
    defineShape();
}

DrawableTriangle::~DrawableTriangle()
{

}

void DrawableTriangle::defineShape()
{
    sf::ConvexShape * cShape = (sf::ConvexShape *) _shape;
    cShape->setPointCount(3);
}

void DrawableTriangle::updateShape()
{
    sf::ConvexShape * cShape = (sf::ConvexShape *) _shape;
		//Actualise la position des points pour toujours dessiner correctement
    cShape->setPoint(0, sf::Vector2f(getPoint(0)->getX(), getPoint(0)->getY()));
    cShape->setPoint(1, sf::Vector2f(getPoint(1)->getX(), getPoint(1)->getY()));
    cShape->setPoint(2, sf::Vector2f(getPoint(2)->getX(), getPoint(2)->getY()));

    Drawable::updateShape();
}

void DrawableTriangle::ecrire(ostream & os) const
{
    os << "DrawableTriangle ";
    Triangle::ecrire(os);
    Drawable::ecrire(os);
}

void DrawableTriangle::moveTo(sf::Vector2i coord) {
	getAncre()->setXY(coord.x - getPoint(1)->getX()/2, coord.y - getPoint(2)->getY()/2);
}
