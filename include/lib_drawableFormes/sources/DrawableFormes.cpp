#include "../headers/DrawableFormes.hpp"

DrawableFormes::DrawableFormes(size_t taille) : _maxFormes(taille), _nbFormes(0), _formes(nullptr)
{
    _formes = new Drawable* [_maxFormes];
}

DrawableFormes::~DrawableFormes()
{
    clean();
}

void DrawableFormes::clean()
{
    for (size_t i = 0; i < _nbFormes; i++)
    {
        delete (_formes[i]);
    }
    delete [] _formes;
}

void DrawableFormes::ajouter(Drawable * const src)
{
    if (_nbFormes >= _maxFormes)
        throw runtime_error("Plus de place");
    else
        _formes[_nbFormes++] = src;
}

Drawable* DrawableFormes::isOver(uint x, uint y) const
{
    for (size_t i = 0; i < _nbFormes; i++)
    {
        if (_formes[i]->isOver(x,y))
          return _formes[i];
				return nullptr;
    }

    throw invalid_argument("Pas de Forme ici");
}

std::ostream& operator<<(std::ostream& os, DrawableFormes const& fs)
{
    fs.sauver(os);
    return os;
}

void DrawableFormes::sauver(ostream & os ) const
{
    os << _maxFormes << " " << _nbFormes << endl;
    for (size_t i = 0; i < _nbFormes; i++)
        os << *(_formes[i]) << endl;
}

void DrawableFormes::charger(istream & is)
{
    clean();
    is >> _maxFormes;
    _formes = new Drawable* [_maxFormes];
    size_t combien;
    is >> combien;
    for (size_t i = 0; i < combien; i++)
        ajouter(Drawable::charger(is));
}

void DrawableFormes::draw(sf::RenderWindow & w) const
{
    for (size_t i = 0; i < _nbFormes; i++)
        (_formes[i])->draw(w);
}
