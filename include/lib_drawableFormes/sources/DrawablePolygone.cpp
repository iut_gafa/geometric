#include "../headers/DrawablePolygone.hpp"


DrawablePolygone::DrawablePolygone(sf::Color const & color, std::vector<Point> const & points, uint x, uint y)
: Forme(x, y),
Drawable(x, y, new sf::ConvexShape(), color),
Polygone(points, x, y)
{
    defineShape();
}


DrawablePolygone::DrawablePolygone(sf::Color const & color, std::vector<Point> const & points, Point * const ancre)
: Forme(ancre),
Drawable(ancre, new sf::ConvexShape(), color),
Polygone(points, ancre)
{
    defineShape();
}


DrawablePolygone::DrawablePolygone(DrawablePolygone const & src) : DrawablePolygone(src.getColor(), *src.getAllPoints(), src.getAncre())
{
    // Pas de defineShape()
}


DrawablePolygone::DrawablePolygone(std::istream & is)
: Forme(is), Drawable(getAncre()->getX(), getAncre()->getY(), new sf::ConvexShape()), Polygone(is)
{
    ulong color;
    is >> color;
    setColor(sf::Color(color));
    defineShape();
}


DrawablePolygone::~DrawablePolygone()
{

}


void DrawablePolygone::defineShape()
{
    sf::ConvexShape * cShape = (sf::ConvexShape *) _shape;
    cShape->setPointCount(getSize());
}

void DrawablePolygone::updateShape()
{
    sf::ConvexShape * cShape = (sf::ConvexShape *) _shape;

    //cShape->setPointCount(getSize()); //Au cas ou on pourrait ajouter des points à chaud
    for (size_t i = 0; i < getSize(); i++)
        cShape->setPoint(i, sf::Vector2f(getPoint(i).getX(), getPoint(i).getY()));

    Drawable::updateShape();
}

void DrawablePolygone::ecrire(ostream & os) const
{
    os << "DrawablePolygone ";
    Polygone::ecrire(os);
    Drawable::ecrire(os);
}

void DrawablePolygone::moveTo(sf::Vector2i coord)
{
	sf::Vector2i centre((_max.getX() - _min.getX())/2 + _min.getX(), (_max.getY() - _min.getY())/2 + _min.getY());
	sf::Vector2i diff = coord - centre;
	for (size_t i = 0; i < getSize(); ++i) {
		Point pointTmp(getPoint(i).getX() + diff.x, getPoint(i).getY() + diff.y);
		setPoint(i, pointTmp);
	}
}
