#include "../headers/DrawablePoint.hpp"

DrawablePoint::DrawablePoint(uint x, uint y)
  : Forme(x, y), Drawable(x, y, new sf::CircleShape(2)), Point(x, y)  {
  defineShape();
}

DrawablePoint::DrawablePoint(Point const& ancre)
  : DrawablePoint(ancre.getX(), ancre.getY()) {
  defineShape();
}

//DrawablePoint::DrawablePoint(std::istream& is) {

//}

DrawablePoint::~DrawablePoint() {}

void DrawablePoint::defineShape() {

}

void DrawablePoint::updateShape() {
  Drawable::updateShape();
}

double DrawablePoint::perimetre() const
{
    return 0;
}

bool DrawablePoint::isOver(uint x, uint y) const
{
    return getX() == x && getY() == y;
}

void DrawablePoint::ecrire(std::ostream& os) const {
  os << "DrawablePoint ";
  os << getX() << " " << getY();
  Drawable::ecrire(os);
}

void DrawablePoint::moveTo(sf::Vector2i coord) {
	setXY(coord.x, coord.y);
}

DrawablePoint::DrawablePoint(DrawablePoint const & src)
: DrawablePoint(src.getX(), src.getY())
{

}

DrawablePoint & DrawablePoint::operator=(DrawablePoint const & src)
{
    DrawablePoint(src.getX(), src.getY());
    return *this;
}
