#include "../headers/DrawableRectangle.hpp"

DrawableRectangle::DrawableRectangle(sf::Color const & couleur, ulong largeur, ulong hauteur, uint x, uint y)
: Forme(x, y),
Drawable(x, y, new sf::RectangleShape(), couleur),
Rectangle(largeur, hauteur, x, y)
{
    defineShape();
}

DrawableRectangle::DrawableRectangle(sf::Color const & couleur, ulong largeur, ulong hauteur, Point * const ancre)
: Forme(ancre),
Drawable(ancre, new sf::RectangleShape(), couleur),
Rectangle(largeur, hauteur, ancre)
{
    defineShape();
}

DrawableRectangle::DrawableRectangle(DrawableRectangle const & src)
: DrawableRectangle(src.getColor(), src.getLargeur(), src.getHauteur(), src.getAncre()->getX(), src.getAncre()->getY())
{

}

DrawableRectangle::DrawableRectangle(std::istream& is)
: Forme(is), Drawable(getAncre()->getX(), getAncre()->getY(), new sf::RectangleShape()), Rectangle(is) {
  ulong color;
  is >> color;
  setColor(sf::Color(color));
  defineShape();
}

DrawableRectangle::~DrawableRectangle()
{

}

void DrawableRectangle::defineShape()
{

}

void DrawableRectangle::updateShape()
{
    sf::RectangleShape * sShape = (sf::RectangleShape *) _shape; // Dynamique Cast pour définir le cercle

    //Actualise pour toujours dessiner au bon endroit
    sShape->setSize(sf::Vector2f(getLargeur(),getHauteur()));
    Drawable::updateShape(); // On appelle la méthode mère
}

void DrawableRectangle::ecrire(std::ostream& os) const {
  os << "DrawableRectangle ";
  Rectangle::ecrire(os);
  Drawable::ecrire(os);
}

void DrawableRectangle::moveTo(sf::Vector2i coord) {
	setAncre(coord.x - getLargeur()/2, coord.y - getHauteur()/2);
}
