#include "../headers/Drawable.hpp"
#include "../headers/DrawableCircle.hpp"
#include "../headers/DrawableEllipsis.hpp"
#include "../headers/DrawableRectangle.hpp"
#include "../headers/DrawableSquare.hpp"
#include "../headers/DrawablePoint.hpp"
#include "../headers/DrawableTriangle.hpp"
#include "../headers/DrawablePolygone.hpp"
#include "../headers/DrawableImage.hpp"

//Constructor
Drawable::Drawable(uint x, uint y, sf::Shape* shape, sf::Color color)
  : Forme(x, y), _selected(false), _thickness(10), _color(color), _shape(shape) {
  _shape -> setFillColor(_color);
  _shape -> setOutlineColor(sf::Color::Red);
}

Drawable::Drawable(Point * const ancre, sf::Shape* shape, sf::Color color) :
  Forme(ancre), _selected(false), _thickness(10), _color(color), _shape(shape) {
  _shape -> setFillColor(_color);
  _shape -> setOutlineColor(sf::Color::Red);
}

//Destructor
Drawable::~Drawable() {
  delete _shape;
}

//Méthode
void Drawable::updatePosition() {
  _shape -> setPosition(getAncre() -> getX(), getAncre() -> getY());
}

void Drawable::draw(sf::RenderWindow& w)
{
    // Si une animation est activé, elle fait ses modifs :
    if (_anims[0])
        animBlink();
    if (_anims[1])
        animRotate();
    if (_anims[2])
        animColor();
    if (_anims[3])
        animBorder();

    if (_animClock.getElapsedTime().asMilliseconds() >= 1000)
    {
        _animClock.restart();
    }

    updateShape(); //Actualise les propriétés de la shape
    w.draw(*_shape);
}

void Drawable::updateShape() {
  updatePosition(); //Replace la shape
  _shape -> setOutlineThickness(_selected ? _thickness : 0); //Si la forme est selectionner on lui rajoute un contour
}

void Drawable::ecrire(std::ostream& os) const
{
  os << " " << _color.toInteger();
}

Drawable* Drawable::charger(istream & is) {
    string type;

    is >> type;

    if (type == "DrawableRectangle") {
      is >> type;
      return new DrawableRectangle(is);
    }
    else if (type == "DrawableSquare") {
      is >> type;
      return new DrawableSquare(is);
    }
    else if (type == "DrawableEllipsis") {
      is >> type;
      return new DrawableEllipsis(is);
    }
    else if (type == "DrawableCircle") {
      is >> type;
      return new DrawableCircle(is);
		}
    else if (type == "DrawableTriangle") {
			is >> type;
			return new DrawableTriangle(is);
    }
		else if (type == "DrawableImage") {
      return new DrawableImage(is);
		}
    else if (type == "DrawablePolygone")
    {
      is >> type;
      return new DrawablePolygone(is);
    }
    else {
      throw out_of_range("Forme non prise en compte");
    }

}
/*void Drawable::Dessiner(sf::RenderWindow & w, bool isActive)
{
    UpdatePos();
    _ancre->Dessiner(w, isActive);
}*/

void Drawable::setAnimEnabled(uint nbAnim, bool state)
{
    try
    {
        if (nbAnim > sizeof(_anims)/sizeof(bool)-1)
            throw nbAnim;
    }
    catch (uint amount)
    {
        std::cerr << std::endl << "Erreur : L'argument state = " << amount << " est invalide."
        << std::endl << "-> Il n'y a que " << sizeof(_anims)/sizeof(bool) << " animations.";
    }

    _anims[nbAnim] = state;

    if (state == false)
    {
        if (nbAnim == 0)
            setColor(_color);
        else if (nbAnim == 1)
            _shape->setRotation(0);
        else if (nbAnim == 2)
            setColor(_color);
        else if (nbAnim == 3)
            _thickness = 10;
    }
}

bool Drawable::isAnimEnabled(uint nbAnim) const
{
    try
    {
        if (nbAnim > sizeof(_anims)/sizeof(bool)-1)
            throw nbAnim;
    }
    catch (uint amount)
    {
        std::cerr << std::endl << "Erreur : L'argument state = " << amount << " est invalide."
        << std::endl << "-> Il n'y a que " << sizeof(_anims)/sizeof(bool) << " animations.";
    }

    return _anims[nbAnim];
}

void Drawable::animBlink()
{
    //std::cerr << _animClock.getElapsedTime().asMilliseconds() % 250  << '\n';
    if (_animClock.getElapsedTime().asMilliseconds() % 50 >= 49)
    {
        _shape->setFillColor(sf::Color(_shape->getFillColor().r, _shape->getFillColor().g, _shape->getFillColor().b, _shape->getFillColor().a +2));
    }
}

void Drawable::animRotate()
{
    if (_animClock.getElapsedTime().asMilliseconds() % 25 >= 24)
    {
        _shape->setRotation(_shape->getRotation() + 1);
    }

}

void Drawable::animColor()
{
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> dis(0, 255);

    if (_animClock.getElapsedTime().asMilliseconds() >= 1000)
    {
        _shape->setFillColor(sf::Color(dis(gen), dis(gen), dis(gen), _shape->getFillColor().a));
    }

}

void Drawable::animBorder()
{
    if (_animClock.getElapsedTime().asMilliseconds() % 50 >= 49)
    {
        if (_thickness >= 100)
        {
            _thickness = 0;
        }
        else
        {
            _thickness++;
        }
    }


}
