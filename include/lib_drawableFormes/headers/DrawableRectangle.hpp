#ifndef DRAWABLE_RECTANGLE_HPP
#define DRAWABLE_RECTANGLE_HPP
//Nos librairies.---------------------------------------------------------------
#include "lib_formes/headers/rectangle.hpp"
#include "Drawable.hpp"

class DrawableRectangle : public Drawable, public Rectangle
{

  virtual void defineShape() override;
	virtual void updateShape() override;

protected:
  void ecrire(ostream &) const override;

public:
  //Constructeur--------------------------------------------------------------
  DrawableRectangle(sf::Color const &, ulong, ulong, uint, uint);
  DrawableRectangle(sf::Color const &, ulong, ulong, Point * const ancre);
  DrawableRectangle(DrawableRectangle const & src);
  DrawableRectangle(std::istream & is);

  //Destructeur---------------------------------------------------------------
  ~DrawableRectangle();

  //Fonction déporté de Rectangle---------------------------------------------
  //void draw(sf::RenderWindow &, bool) override;
  //void setColor(sf::Color const & c) override;
	//Méthodes
	virtual void moveTo(sf::Vector2i coord) override;

};


#endif /* end of include guard: DRAWABLE_RECTANGLE_HPP */
