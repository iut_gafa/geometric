#ifndef DRAWABLE_CIRCLE_HPP
#define DRAWABLE_CIRCLE_HPP
//Nos librairies.---------------------------------------------------------------
#include "lib_formes/headers/cercle.hpp"
#include "Drawable.hpp"

class DrawableCircle : public Drawable, public Cercle
{

  virtual void defineShape() override;
	virtual void updateShape() override;

protected:
  void ecrire(ostream &) const override;

public:
  //Constructeur--------------------------------------------------------------
  DrawableCircle(sf::Color const &, ulong, uint, uint);
  DrawableCircle(sf::Color const &, ulong, Point * const ancre);
  DrawableCircle(DrawableCircle const & src);
  DrawableCircle(std::istream& is);
  DrawableCircle() = delete;

  //Destructeur---------------------------------------------------------------
  ~DrawableCircle();

  //Fonction déporté de cercle------------------------------------------------
  //void draw(sf::RenderWindow &, bool) override;
	//Méthode
	virtual void moveTo(sf::Vector2i coord) override;
};

#endif /* end of include guard: DRAWABLE_CIRCLE_HPP */
