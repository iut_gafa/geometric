#ifndef DRAWABLE_SFML_HPP
#define DRAWABLE_SFML_HPP

#include <random>

//SFML--------------------------------------------------------------------------
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

//My Lib------------------------------------------------------------------------
#include "lib_formes/headers/forme.hpp"

class Drawable : virtual public Forme {
    //Attribut------------------------------------------------------------------
    bool _selected;
    bool _anims[4] {false, false, false, false};
    //bool _pulseSide;
    uint _thickness;
    sf::Color _color;
    sf::Clock _animClock = sf::Clock();

    //Fonction qui faisais partie de forme--------------------------------------
    void updatePosition();
    void animBlink();
    void animRotate();
    void animColor();
    void animBorder();
    //void animPulse();

  protected:
    sf::Shape* _shape;

    virtual void defineShape() = 0; //Permet de définir la shape à la création
		virtual void updateShape(); //Permet d'actualiser la shape à chaque tour de boucle
    virtual void ecrire(ostream &) const override;


  public:
    //constructeur--------------------------------------------------------------
    Drawable() = delete;
    Drawable(uint x, uint y, sf::Shape* _shape, sf::Color _color = sf::Color::Black);
    Drawable(Point * const ancre, sf::Shape* _shape, sf::Color _color = sf::Color::Black);
    Drawable(Drawable const& src) = delete;
    Drawable(std::istream& is);

    //Destructeur---------------------------------------------------------------
    ~Drawable();

    //Ici on déporte toutes les fonctions de forme qui servent pour le dessin.--
    virtual void draw(sf::RenderWindow&);
    static Drawable* charger(std::istream& is);
	virtual void moveTo(sf::Vector2i coord) = 0;
    void setEnabled(bool state) { _shape->setFillColor(state ? _color : sf::Color::Transparent);};

    //Getteur / Setteur
    inline sf::Color getColor() const { return _shape -> getFillColor(); };
    inline void setColor(sf::Color const& c) { _color = c; _shape->setFillColor(_color); };
    inline bool isSelected() const { return _selected; };
    inline void setSelected(bool b) { _selected = b; };
    inline uint getThickness() const { return _thickness; };
    inline void setThickness(uint size) { _thickness = size; };
    void setAnimEnabled(uint nbAnim, bool state);
    bool isAnimEnabled(uint nbAnim) const;
};

#endif /* end of include guard: DRAWABLE_SFML_HPP */
