#ifndef DRAWABLE_ELLIPSIS_HPP
#define DRAWABLE_ELLIPSIS_HPP
//Notre lib_formes
#include "lib_formes/headers/ellipse.hpp"
#include "Drawable.hpp"

class DrawableEllipsis : public Drawable, public Ellipse
{

private:
  virtual void defineShape() override;
	virtual void updateShape() override;

protected:
  void ecrire(ostream &) const override;

public:
  //Constructeur--------------------------------------------------------------
  DrawableEllipsis(sf::Color const &, ulong, ulong, uint, uint);
  DrawableEllipsis(sf::Color const &, ulong, ulong, Point * const ancre);
  DrawableEllipsis(DrawableEllipsis const &);
  DrawableEllipsis(std::istream&);
  DrawableEllipsis() = delete;

  //Destructeur---------------------------------------------------------------
  ~DrawableEllipsis();

  //Methodes------------------------------------------------------------------
  //void draw(sf::RenderWindow &, bool) override;
  //void setColor(sf::Color const & c) override;
	virtual void moveTo(sf::Vector2i coord) override;

};

#endif /* end of include guard: DRAWABLE_ELLIPSIS_HPP */
