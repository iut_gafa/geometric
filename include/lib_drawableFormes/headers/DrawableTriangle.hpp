#ifndef DRAWABLE_TRIANGLE_HPP
#define DRAWABLE_TRIANGLE_HPP
#include "../../lib_formes/headers/triangle.hpp"
#include "Drawable.hpp"

class DrawableTriangle : public Drawable, public Triangle
{

private:
    virtual void defineShape() override;
		virtual void updateShape() override;

protected:
    void ecrire(ostream &) const override;

public:
    //Constructeur
    DrawableTriangle(sf::Color const &, Point * const, Point * const, Point * const, uint, uint);
    DrawableTriangle(sf::Color const &, Point * const, Point * const, Point * const, Point * const ancre);
    DrawableTriangle(DrawableTriangle const &); //Constructeur de copie
    DrawableTriangle(std::istream &);

    //Destructeur
    ~DrawableTriangle();

    //Fonctions virtuels de Drawable--------------------------------------------
		void moveTo(sf::Vector2i coord) override;
};

#endif /* end of include guard: DRAWABLE_TRIANGLE_HPP */
