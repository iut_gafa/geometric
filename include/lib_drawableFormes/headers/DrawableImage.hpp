#ifndef DRAWABLE_IMAGE_HPP
#define DRAWABLE_IMAGE_HPP

//SFML--------------------------------------------------------------------------
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

//My Lib------------------------------------------------------------------------
#include "Drawable.hpp"

/// Permet de manipuler une image dans une fenêtre
class DrawableImage : public Drawable
{
private:
    sf::Texture * _texture;
    //sf::Sprite * _sprite;
    uint _height, _width;
    std::string _filename;
    bool _fixScale;
    double _scale;
    void setNewScale();
		void defineShape() override;
		void updateShape() override;

protected:
	void ecrire(std::ostream& os) const override;

public:
		/*! Construit une image par défaut. */
    DrawableImage();

    DrawableImage(const std::string & filename, uint width, uint height, uint x, uint y);

    DrawableImage(const std::string & filename, uint width, uint height, Point * const ancre);

    DrawableImage(DrawableImage const & src);

		DrawableImage(std::istream& is);

    // Méthodes non-affichées :
    ~DrawableImage();
    // - - - - - - -

    /*! Permet de définir si l'image doit garder la même proportion. */
    inline void setFixScale(bool b /** [in] True pour activer le maintient de la proportion, False pour le désactiver. */) {_fixScale = b;};

    /*! Renvoie si la proportion de l'image est bloquée ou non. */
    inline bool isScaleFixed() const {return _fixScale;};

    inline uint getHeight() const {return _height;};
    void setHeight(uint size);
    inline uint getWidth() const {return _width;};
    void setWidth(uint size);

    void setAlpha(sf::Uint8 amount);
    int getAlpha() const {return int(getColor().a);};

    double perimetre() const;

    bool isOver(uint, uint) const;
		void moveTo(sf::Vector2i coord) override;



};

#endif /* end of include guard: DRAWABLE_IMAGE_HPP */
