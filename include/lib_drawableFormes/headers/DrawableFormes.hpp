#ifndef DRAWABLE_FORMES_HPP
#define DRAWABLE_FORMES_HPP

#include "Drawable.hpp"
#include <cstddef>

class DrawableFormes
{
private:
    size_t _maxFormes;
    size_t _nbFormes;
    Drawable ** _formes;

public:
    DrawableFormes(size_t = 10);
    DrawableFormes(DrawableFormes const& src) = delete;
    ~DrawableFormes();
    void clean();
    void ajouter(Drawable * const);
    Drawable* isOver(uint, uint) const;
    friend ostream& operator<<(ostream&, DrawableFormes const&);
    void sauver(ostream &) const;
    void charger(istream &);
    void draw(sf::RenderWindow &) const;

};

#endif /* end of include guard: DRAWABLE_FORMES_HPP */
