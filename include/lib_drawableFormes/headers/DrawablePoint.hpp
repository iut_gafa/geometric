#ifndef DRAWABLE_POINT_HPP
#define DRAWABLE_POINT_HPP
//Nos lib-----------------------------------------------------------------------
#include "lib_formes/headers/point.hpp"
#include "Drawable.hpp"

class DrawablePoint : public Drawable, public Point {

  virtual void defineShape() override;
	void updateShape() override;

protected:
  void ecrire(ostream &) const override;

public:
  //Constructeur--------------------------------------------------------------
  DrawablePoint() = delete;
  DrawablePoint(uint x, uint y);
  DrawablePoint(Point const& ancre);
  //DrawablePoint(istream & is);
  DrawablePoint(DrawablePoint const & src);
  DrawablePoint & operator=(DrawablePoint const & src);

  //Destructeur---------------------------------------------------------------
  ~DrawablePoint();

  //Opérateurs----------------------------------------------------------------
  //DrawablePoint& operator=(Point const &);

  //Méthodes------------------------------------------------------------------
  //void draw(sf::RenderWindow&, bool) const;
  virtual double perimetre() const;
  virtual bool isOver(uint, uint) const;
	virtual void moveTo(sf::Vector2i coord) override;
};

#endif /* end of include guard: DRAWABLE_POINT_HPP */
