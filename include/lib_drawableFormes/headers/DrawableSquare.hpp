#ifndef DRAWABLE_SQUARE_HPP
#define DRAWABLE_SQUARE_HPP
#include "lib_formes/headers/carre.hpp"
#include "Drawable.hpp"

class DrawableSquare : public Drawable, public Carre {

    virtual void defineShape() override;
		virtual void updateShape() override;

protected:
    void ecrire(ostream &) const override;

public:
    //Constructeur
    DrawableSquare(sf::Color const& color, ulong cote, uint x, uint y); //Ils appellent les constructeur de DrawableItem pour la couleur.
    DrawableSquare(sf::Color const&, ulong cote, Point * const ancre); //Et de Carre pour le reste.
    DrawableSquare(DrawableSquare const & src); //Constructeur de copie
    DrawableSquare(std::istream& is);

    //Destructeur
    ~DrawableSquare();

  //Fonctions virtuels de Drawable----------------------------------------------
	virtual void moveTo(sf::Vector2i coord) override;
};

#endif /* end of include guard: DRAWABLE_SQUARE_HPP */
