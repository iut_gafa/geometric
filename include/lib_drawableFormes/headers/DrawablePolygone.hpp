#ifndef DRAWABLE_POLYGONE_HPP
#define DRAWABLE_POLYGONE_HPP
#include "../../lib_formes/headers/polygone.hpp"
#include "Drawable.hpp"

class DrawablePolygone : public Drawable, public Polygone
{

private:
    virtual void defineShape() override;
		virtual void updateShape() override;

protected:
    void ecrire(ostream &) const override;

public:
    //Constructeur
    DrawablePolygone(sf::Color const & color, std::vector<Point> const & points, uint, uint);
    DrawablePolygone(sf::Color const & color, std::vector<Point> const & points, Point * const ancre);
    DrawablePolygone(DrawablePolygone const & src); //Constructeur de copie
    DrawablePolygone(std::istream & is);

    //Destructeur
    ~DrawablePolygone();

    void moveTo(sf::Vector2i coord);
};

#endif /* end of include guard: DRAWABLE_POLYGONE_HPP */
